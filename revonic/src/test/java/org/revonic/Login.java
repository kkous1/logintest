package org.revonic;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import excel.Readexcel;
import page_Factory.Loginpage;

public class Login {
	public WebDriver driver;
	public WebDriverWait wait;
	public String foldername = System.getProperty("user.dir") + "/src/test/java/data/";
	public String excelpath = System.getProperty("user.dir") + "/src/test/java/data/Data.xlsx";
	String url = "https://courses.ultimateqa.com/users/sign_in";
	Loginpage objlogin;
	Readexcel excel;

	@Parameters("browser")

	@BeforeTest

	public void TestSetup(String browser) {

		// If the browser is Firefox, then do this

		if (browser.equalsIgnoreCase("firefox")) {

			System.setProperty("webdriver.gecko.driver", foldername + "geckodriver.exe");
			driver = new FirefoxDriver();

			// If browser is chrome, then do this

		} else if (browser.equalsIgnoreCase("chrome")) {

			// Here I am setting up the path for my chrome

			System.setProperty("webdriver.chrome.driver", foldername + "chromedriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			driver = new ChromeDriver(capabilities);

		}

		driver.manage().window().maximize();
		objlogin = new Loginpage(driver);
		wait = new WebDriverWait(driver, 10);
		excel = new Readexcel();
	}

	@BeforeMethod
	public void geturl() {
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test(priority = 0)
	public void loginwithemptyfields() {
		// click on login button
		objlogin.clickLogin();
		objlogin.verifyErrormessage(wait, "Invalid email or password.");
	}

	@Test(priority = 1)
	public void loginwithoutemail() {

		objlogin.sendPassword("Test@123");
		objlogin.clickLogin();
		objlogin.verifyErrormessage(wait, "Invalid email or password.");
	}

	@Test(priority = 2)
	public void loginwithoutpassword() {
		// click on login button

		objlogin.sendEmailID("koushi22k@gmail.com");
		objlogin.clickLogin();
		objlogin.verifyErrormessage(wait, "Invalid email or password.");
	}

	@Test(priority = 3)
	public void Resetpasswordpage() {

		objlogin.clickresetpwd();
		objlogin.verifyelementpresent(wait, objlogin.resetemailId);
		objlogin.verifyelementpresent(wait, objlogin.sendbtn);

	}

	@DataProvider

	public Object[][] credentials() throws Exception {

		Object[][] list = excel.getTableArray(excelpath, "dataset");
		return list;
	}

	@Test(dataProvider = "credentials", priority = 4)
	public void incorrectemailcombination(String email, String password) {

		objlogin.loginTowebsite(email, password);
		if (email.contains(".")) {
			objlogin.verifyHtml5EmailError(wait);
		} else {
			objlogin.verifyErrormessage(wait, "Invalid email or password.");
		}

	}

	@Test(priority = 5)
	public void successlogin() {

		objlogin.loginTowebsite("test1@test.com", "Test@123");
		objlogin.verifyelementpresent(wait, objlogin.successloginmsg);

	}

	@AfterTest
	public void quit() {
		driver.quit();
	}
}