package page_Factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Loginpage {

	WebDriver driver;
	WebDriverWait wait;

	@FindBy(xpath = ".//input[@id='user_email']")
	public WebElement emailId;

	@FindBy(css = "input#user_email:invalid")
	public WebElement emailId1;

	@FindBy(id = "user_password")
	public WebElement password;

	@FindBy(xpath = ".//input[@id='btn-signin']")
	public WebElement submitbtn;

	@FindBy(xpath = ".//*[@id='notifications-error']/ul/li")
	public WebElement error;

	@FindBy(xpath = "//a[@class='forgot-password']")
	public WebElement resetpassword;

	@FindBy(id = "user_email")
	public WebElement resetemailId;

	@FindBy(xpath = "//input[@type='submit']")
	public WebElement sendbtn;

	@FindBy(xpath = "//a[@id='my_account']")
	public WebElement successloginmsg;

	@FindBy(id = "")
	public WebElement mainmenu;

	public Loginpage(WebDriver driver) {
		this.driver = driver;

		// This initElements method will create all WebElements

		PageFactory.initElements(driver, this);
	}

	// Set user name in textbox
	public void sendEmailID(String strEmailId) {
		emailId.sendKeys(strEmailId);

	}

	// Set password in password textbox
	public void sendPassword(String strPassword) {
		password.sendKeys(strPassword);
	}

	// Click on login button
	public void clickLogin() {
		submitbtn.click();
	}

	// Click on resetpassword button
	public void clickresetpwd() {
		resetpassword.click();
	}

	// Get the title of Login Page
	public String getcssvalue() {
		return emailId.getCssValue("");
	}

	public void verifybordercolor(WebElement loc, String expcssValue) {
		String color = loc.getCssValue("border-color");
		Assert.assertEquals(color, expcssValue);
	}

	public void verifyErrormessage(WebDriverWait wait, String expected) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(error));
		String actual = error.getText();
		Assert.assertEquals(actual, expected);

	}

	public void verifyHtml5EmailError(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(emailId1));
		String actual = emailId1.getAttribute("validationMessage");
		Assert.assertTrue(actual.contains("email address."));

		// Assert.assertTrue(emailId1.isDisplayed(), "Please enter an email
		// address.");

	}

	public void verifyelementpresent(WebDriverWait wait, WebElement loc) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(loc));
		Assert.assertEquals(loc.isDisplayed(), true);

	}

	public void loginTowebsite(String strUserName, String strPasword) {
		// Fill user name
		this.sendEmailID(strUserName);
		// Fill password
		this.sendPassword(strPasword);
		// Click Login button
		this.clickLogin();

	}
}
